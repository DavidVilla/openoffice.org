# Coloca este fichero en ~/.openoffice.org2/user/Scripts/python/

import os

def serviceManager():
    return XSCRIPTCONTEXT.getComponentContext().getServiceManager()    


# The CoreReflection object. It is cached in a global variable.
goCoreReflection = None
def getCoreReflection():
    global goCoreReflection
    if not goCoreReflection:
        goCoreReflection = serviceManager().createInstance("com.sun.star.reflection.CoreReflection")
    return goCoreReflection


def createUnoStruct(cTypeName):
    """Create a UNO struct and return it.
    Similar to the function of the same name in OOo Basic."""
    # Get the IDL class for the type name
    oXIdlClass = getCoreReflection().forName(cTypeName)
    # Create the struct.
    oReturnValue, oStruct = oXIdlClass.createObject(None)
    return oStruct 


def pathnameToUrl(cPathname):
    """Convert a Windows or GNU/Linux pathname into an OOo URL."""
    if len( cPathname ) > 1 and cPathname[1:2] == ":":
        cPathname = "/" + cPathname[0] + "|" + cPathname[2:]
    return "file://" + cPathname.replace("\\", "/")


def openURL(cUrl, tProperties=()):
    """Open or Create a document from it's URL."""
    oDesktop = XSCRIPTCONTEXT.getDesktop()
    return oDesktop.loadComponentFromURL(cUrl, "_blank", 0, tProperties)
  

def MakePropertyValue( cName=None, uValue=None, nHandle=None, nState=None ):
    """Create a com.sun.star.beans.PropertyValue struct and return it."""
    oPropertyValue = createUnoStruct("com.sun.star.beans.PropertyValue")

    if cName != None:
        oPropertyValue.Name = cName
    if uValue != None:
        oPropertyValue.Value = uValue
    if nHandle != None:
        oPropertyValue.Handle = nHandle
    if nState != None:
        oPropertyValue.State = nState

    return oPropertyValue 


def main(dummy):
    cSourceFile = os.environ['OOARG']
    cSourceURL = pathnameToUrl( cSourceFile )

    cTargetFile = os.path.splitext(cSourceFile)[0] + '.pdf'
    cTargetURL = pathnameToUrl( cTargetFile )

    # Open the source document.
    # No filter necessary.  OOo will figure it out from the .DOC extension.
    oDoc = openURL(cSourceURL, (MakePropertyValue("Hidden", True),))

    # Save the newly opened document.
    oDoc.storeToURL(cTargetURL,
                    (MakePropertyValue("FilterName","writer_pdf_Export"),))
    oDoc.dispose()


g_exportedScripts = main,
